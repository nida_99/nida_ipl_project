const matchesPlayedPerYear = require('./ipl.js').matchesPlayedPerYear;
const matchesWonPerYear = require('./ipl.js').matchesWonPerYear;
const getIdOfYear = require('./ipl.js').getIdOfYear;
const extraRunConceded = require('./ipl.js').extraRunConceded;
const topEconomyOfBowler = require('./ipl.js').topEconomyOfBowler;
const wonToss = require('./ipl.js').wonToss;
const playerOfTheMatch = require('./ipl.js').playerOfTheMatch;
const calculateStrikeRate = require('./ipl.js').calculateStrikeRate;
const findSeason = require('./ipl.js').findSeason;
const playerDismissed = require('./ipl.js').playerDismissed;
const superOverEconomy = require('./ipl.js').superOverEconomy; 


const csv = require('csv-parser');
const fs = require('fs');

const matches = [];
const deliveries = [];

fs.createReadStream('../data/matches.csv')
    .pipe(
        csv({}))
    .on('data', (data) => {
        matches.push(data)
    }).on('end', () => {
        const matchPlayed = matchesPlayedPerYear(matches);


        fs.writeFileSync('../public/output/matchesPlayedPerYear.json', JSON.stringify(matchPlayed), "utf-8", (err) => {
            if (err) {
                throw err;
            };
        });

        const matchesWon = matchesWonPerYear(matches);


        fs.writeFileSync('../public/output/matchesWonPerTeam.json', JSON.stringify(matchesWon), "utf-8", (err) => {
            if (err) {
                throw err;
            };
        });

        const wonMatchAndToss = wonToss(matches);
        fs.writeFileSync('../public/output/wonMatchAndToss.json', JSON.stringify(wonMatchAndToss ), "utf-8", (err) => {
            if (err) {
                throw err;
            };
        });

        const matchPlayer = playerOfTheMatch(matches);
        fs.writeFileSync('../public/output/PlayerOfTheMatch.json', JSON.stringify(matchPlayer), "utf-8", (err) => {
            if (err) {
                throw err;
            };
        });

    });

fs.createReadStream('../data/deliveries.csv')
    .pipe(
        csv({}))
    .on('data', (data) => {
        deliveries.push(data)

    }).on('end', () => {

        const matchedId = getIdOfYear(matches, 2016);
        const extraRuns = extraRunConceded(deliveries, matchedId);
    

        fs.writeFileSync('../public/output/extraRunConceded.json', JSON.stringify(extraRuns), "utf-8", (err) => {
            if (err) {
                throw err;
            };
        });

        const matchid = getIdOfYear(matches, 2015);
        const economyBowler = topEconomyOfBowler(deliveries, matchid);


        fs.writeFileSync('../public/output/topEconomyBowler.json', JSON.stringify(economyBowler), "utf-8", (err) => {
            if (err) {
                throw err;
            };

        });

        const strikeRate = calculateStrikeRate(deliveries, matches);
        
        fs.writeFileSync('../public/output/strikeRateOfBatsman.json', JSON.stringify(strikeRate), "utf-8", (err) => {
            if (err) {
                throw err;
            };

        });

        const dismissedPlayer = playerDismissed(deliveries);

        fs.writeFileSync('../public/output/HighestTimePlayerDismissed.json', JSON.stringify(dismissedPlayer), "utf-8", (err) => {
            if (err) {
                throw err;
            };

        });

        const superOver = superOverEconomy(deliveries);
        fs.writeFileSync('../public/output/superOverEconomy.json', JSON.stringify(superOver), "utf-8", (err) => {
            if (err) {
                throw err;
            };

        });


    });

