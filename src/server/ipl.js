// 1.  Number of matches played per year for all the years in IPL.

function matchesPlayedPerYear(input) {
  const matchPerYear = input.reduce((matchesPlayed, current) => {
    if (matchesPlayed[current.season]) {
      matchesPlayed[current.season] += 1;
    } else {
      matchesPlayed[current.season] = 1;
    }
    return matchesPlayed;
  }, {});

  return matchPerYear;
}

// 2. Number of matches won per team per year in IPL.

function matchesWonPerYear(input) {
  const matchWonperTeam = input.reduce((matchesWon, current) => {
    if (matchesWon[current.winner]) {
      if (matchesWon[current.winner][current.season]) {
        matchesWon[current.winner][current.season] += 1;
      } else {
        matchesWon[current.winner][current.season] = 1;
      }
    } else {
      matchesWon[current.winner] = {};
      matchesWon[current.winner][current.season] = 1;
    }
    return matchesWon;
  }, {});
  return matchWonperTeam;
}

// 3. Extra runs conceded per team in the year 2016

function getIdOfYear(input, season) {
  return input
    .filter((element) => element.season == season)
    .map((ele) => ele.id);
}

function extraRunConceded(input, matchedId) {
  const extraRunsConceded = input.reduce((extraRuns, deliveries) => {
    if (matchedId.includes(deliveries.match_id)) {
      if (extraRuns[deliveries.bowling_team]) {
        extraRuns[deliveries.bowling_team] += parseInt(deliveries.extra_runs);
      } else {
        extraRuns[deliveries.bowling_team] = parseInt(deliveries.extra_runs);
      }
    }
    return extraRuns;
  }, {});
  return extraRunsConceded;
}

// 4. Top 10 economical bowlers in the year 2015

function topEconomyOfBowler(input, matchedId) {
  let economicalBowler = input.reduce((economy, deliveries) => {
    if (matchedId.includes(deliveries.match_id)) {
      if (economy[deliveries.bowler]) {
        if (economy[deliveries.bowler].run) {
          economy[deliveries.bowler].run += parseInt(deliveries.total_runs);
        } else {
          economy[deliveries.bowler].run = parseInt(deliveries.total_runs);
        }
        if (economy[deliveries.bowler].balls) {
          if (deliveries.wide_runs === "0" && deliveries.noball_runs === "0") {
            economy[deliveries.bowler].balls += 1;
          }
        } else {
          if (deliveries.wide_runs === "0" && deliveries.noball_runs === "0") {
            economy[deliveries.bowler].balls = 1;
          }
        }
      } else {
        economy[deliveries.bowler] = {};
      }
    }

    return economy;
  }, {});
  let economyObject = {};
  Object.keys(economicalBowler).map((element) => {
    let value = (
      (economicalBowler[element].run * 6) /
      economicalBowler[element].balls
    ).toFixed(5);
    economyObject[element] = value;
  });

  let sortedObj = Object.entries(economyObject).sort((a, b) => {
    return a[1] - b[1];
  });
  return sortedObj.slice(0, 10);
}

//5. Find the number of times each team won the toss and also won the match

function wonToss(input) {
  return input.reduce((wonTheMatch, current) => {
    if (wonTheMatch[current.winner]) {
      if (current.winner === current.toss_winner) {
        wonTheMatch[current.winner] += 1;
      } else {
        wonTheMatch[current.winner] = 1;
      }
    } else {
      wonTheMatch[current.winner] = {};
    }
    return wonTheMatch;
  }, {});
}

// 6. Find a player who has won the highest number of Player of the Match awards for each season

function playerOfTheMatch(array) {
  return array.reduce((acc, current) => {
    if (!acc[current.season]) {
      acc[current.season] = {};

      let players = {};

      array.map((element) => {
        if (element.season === current.season) {
          if (players[element.player_of_match]) {
            players[element.player_of_match] += 1;
          } else {
            players[element.player_of_match] = 1;
          }
        }
      });

      TopPlayer = Object.keys(players).reduce((a, b) =>
        players[a] > players[b] ? a : b
      );
      acc[current.season] = TopPlayer;
    }
    return acc;
  }, {});
}

// 7. Find the strike rate of a batsman for each season

function findSeason(input, ids) {
  let matchingID = input.filter((element) => element.id == ids);
  return matchingID[0].season;
}

function calculateStrikeRate(input1, input2) {
  let value = input1.reduce((acc, curr) => {
    let seasonMatch = findSeason(input2, curr.match_id);
    if (acc[seasonMatch]) {
      if (acc[seasonMatch][curr.batsman]) {
        if (acc[seasonMatch][curr.batsman].ball) {
          if (curr.wide_runs === "0" && curr.noball_runs === "0") {
            acc[seasonMatch][curr.batsman].ball += 1;
          }
        } else {
          if (curr.wide_runs === "0" && curr.noball_runs === "0") {
            acc[seasonMatch][curr.batsman].ball = 1;
          }
        }
        if (acc[seasonMatch][curr.batsman].runs) {
          acc[seasonMatch][curr.batsman].runs += parseInt(curr.batsman_runs);
        } else {
          acc[seasonMatch][curr.batsman].runs = parseInt(curr.batsman_runs);
        }
        if (acc[seasonMatch][curr.batsman].strike_rate) {
          acc[seasonMatch][curr.batsman].strike_rate = (
            (acc[seasonMatch][curr.batsman].runs /
              acc[seasonMatch][curr.batsman].ball) *
            100
          ).toFixed(4);
        } else {
          acc[seasonMatch][curr.batsman].strike_rate = 1;
        }
      } else {
        acc[seasonMatch][curr.batsman] = {};
      }
    } else {
      acc[seasonMatch] = {};
    }
    return acc;
  }, {});

  let batsman_strike_rate = {};
  Object.keys(value).map((season) => {
    Object.keys(value[season]).map((batsman) => {
      if (batsman_strike_rate[batsman] === undefined) {
        batsman_strike_rate[batsman] = { year: 0, s_rate: 0 };
      }
      batsman_strike_rate[batsman].year = season;
      batsman_strike_rate[batsman].s_rate =
        value[season][batsman]["strike_rate"];
    });
  });
  return batsman_strike_rate;
}

// 8. Find the highest number of times one player has been dismissed by another player

function playerDismissed(input) {
  return input.reduce((acc, current) => {
    if (!acc[current.player_dismissed]) {
      if (current.player_dismissed !== "") {
        acc[current.player_dismissed] = {};

        let players = {};

        input.map((element) => {
          if (element.player_dismissed === current.player_dismissed) {
            if (players[element.bowler]) {
              players[element.bowler] += 1;
            } else {
              players[element.bowler] = 1;
            }
          }
        });

        HighestDismissed = Object.keys(players).reduce((a, b) =>
          players[a] > players[b] ? a : b
        );
        acc[current.player_dismissed] = HighestDismissed;
      }
    }
    return acc;
  }, {});
}
// 9. Find the bowler with the best economy in super overs

function superOverEconomy(input) {
  let superEconomy = input.reduce((economy, deliveries) => {
    if (deliveries.is_super_over === "1") {
      if (economy[deliveries.bowler]) {
        if (economy[deliveries.bowler].run) {
          economy[deliveries.bowler].run += parseInt(deliveries.total_runs);
        } else {
          economy[deliveries.bowler].run = parseInt(deliveries.total_runs);
        }
        if (economy[deliveries.bowler].balls) {
          if (deliveries.wide_runs === "0" && deliveries.noball_runs === "0") {
            economy[deliveries.bowler].balls += 1;
          }
        } else {
          if (deliveries.wide_runs === "0" && deliveries.noball_runs === "0") {
            economy[deliveries.bowler].balls = 1;
          }
        }
      } else {
        economy[deliveries.bowler] = {};
      }
    }
    return economy;
  }, {});

  let economyOfBowler = Object.entries(superEconomy);
  let val = economyOfBowler.reduce((acc, curr) => {
    acc[curr[0]] = (curr[1].run * 6) / curr[1].balls;
    return acc;
  }, {});
  input = Object.entries(val);
  input = input.sort((a, b) => a[1] - b[1]);
  return input.slice(0, 1);
}

module.exports.matchesPlayedPerYear = matchesPlayedPerYear;
module.exports.matchesWonPerYear = matchesWonPerYear;
module.exports.getIdOfYear = getIdOfYear;
module.exports.extraRunConceded = extraRunConceded;
module.exports.topEconomyOfBowler = topEconomyOfBowler;
module.exports.wonToss = wonToss;
module.exports.playerOfTheMatch = playerOfTheMatch;
module.exports.calculateStrikeRate = calculateStrikeRate;
module.exports.findSeason = findSeason;
module.exports.playerDismissed = playerDismissed;
module.exports.superOverEconomy = superOverEconomy;
